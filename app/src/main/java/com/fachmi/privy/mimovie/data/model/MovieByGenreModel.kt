package com.fachmi.privy.mimovie.data.model

import android.os.Parcelable
import com.fachmi.privy.mimovie.data.response.MovieByGenreResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieByGenreModel(
    val movieId: Int,
    val movieTitle: String,
    val movieImage: String,
) : Parcelable

fun MovieByGenreResponse.toMovieByGenreList(): MutableList<MovieByGenreModel> {
    return this.results?.map { response ->
        MovieByGenreModel(
            movieId = response?.id ?: 0,
            movieTitle = response?.title.orEmpty(),
            movieImage = response?.posterPath.orEmpty()
        )
    }.orEmpty().toMutableList()
}