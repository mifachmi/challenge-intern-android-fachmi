package com.fachmi.privy.mimovie.data.network

import com.fachmi.privy.mimovie.BuildConfig
import com.fachmi.privy.mimovie.data.response.GenreResponse
import com.fachmi.privy.mimovie.data.response.MovieByGenreResponse
import com.fachmi.privy.mimovie.data.response.MovieDetailResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("genre/movie/list?api_key=${BuildConfig.MOVIE_API_KEY}")
    fun getAllGenre(): Call<GenreResponse>

    @GET("discover/movie?api_key=${BuildConfig.MOVIE_API_KEY}&page=1")
    fun getMovieByGenre(
        @Query("with_genres") withGenres: String
    ): Call<MovieByGenreResponse>

    @GET("movie/{movie_id}?api_key=${BuildConfig.MOVIE_API_KEY}")
    fun getMovieDetail(
        @Path("movie_id") movieId: Int
    ): Call<MovieDetailResponse>
}