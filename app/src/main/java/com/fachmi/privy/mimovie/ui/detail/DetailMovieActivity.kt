package com.fachmi.privy.mimovie.ui.detail

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.fachmi.privy.mimovie.common.viewmodel.DetailMovieViewModel
import com.fachmi.privy.mimovie.common.viewmodel.ViewModelFactory
import com.fachmi.privy.mimovie.data.model.MovieByGenreModel
import com.fachmi.privy.mimovie.data.model.toDetailMovie
import com.fachmi.privy.mimovie.data.response.ApiResponse
import com.fachmi.privy.mimovie.databinding.ActivityDetailMovieBinding

class DetailMovieActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailMovieBinding

    private lateinit var viewModel: DetailMovieViewModel
    private lateinit var movieModel: MovieByGenreModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViewModel()
        getExtras()
        populateDataMovie()
    }

    private fun getExtras() {
        intent.apply {
            movieModel = getParcelableExtra("EXTRA_MOVIE")!!
            viewModel.movieId = movieModel.movieId
        }
    }

    private fun initViewModel() {
        val factory = ViewModelFactory.getInstance(this)
        viewModel = ViewModelProvider(this, factory)[DetailMovieViewModel::class.java]
    }

    private fun populateDataMovie() {
        viewModel.getDetailMovie().observe(this) { result ->
            when (result) {
                is ApiResponse.Loading -> {
                    Toast.makeText(this, "loading...", Toast.LENGTH_SHORT).show()
                }
                is ApiResponse.Success -> {
                    val data = result.data.toDetailMovie()
                    binding.apply {
                        Glide.with(this@DetailMovieActivity)
                            .load("https://image.tmdb.org/t/p/original${data.movieImage}")
                            .into(ivMovieDetail)
                        tvMovieTitleDetail.text = data.movieTitle
                        tvMovieOverviewDetail.text = data.movieOverview
                    }
                }
                is ApiResponse.Error -> {
                    Toast.makeText(this, result.errorMessage, Toast.LENGTH_SHORT).show()
                }
                else -> {}
            }
        }
    }
}