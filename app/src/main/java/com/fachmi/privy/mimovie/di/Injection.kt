package com.fachmi.privy.mimovie.di

import android.content.Context
import com.fachmi.privy.mimovie.data.network.ApiConfig
import com.fachmi.privy.mimovie.data.repository.DetailMovieRepository
import com.fachmi.privy.mimovie.data.repository.GenreRepository
import com.fachmi.privy.mimovie.data.repository.MovieByGenreRepository
import com.fachmi.privy.mimovie.data.source.RemoteDataSource

object Injection {

    fun provideGenreRepository(context: Context): GenreRepository {
        val apiService = ApiConfig.provideApiService(context)
        val remoteDataSource = RemoteDataSource.getInstance(apiService)

        return GenreRepository.getInstance(remoteDataSource)
    }

    fun provideMovieByGenreRepository(context: Context): MovieByGenreRepository {
        val apiService = ApiConfig.provideApiService(context)
        val remoteDataSource = RemoteDataSource.getInstance(apiService)

        return MovieByGenreRepository.getInstance(remoteDataSource)
    }

    fun provideDetailMovieRepository(context: Context): DetailMovieRepository {
        val apiService = ApiConfig.provideApiService(context)
        val remoteDataSource = RemoteDataSource.getInstance(apiService)

        return DetailMovieRepository.getInstance(remoteDataSource)
    }
}