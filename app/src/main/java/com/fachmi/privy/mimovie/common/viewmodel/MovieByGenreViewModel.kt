package com.fachmi.privy.mimovie.common.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.fachmi.privy.mimovie.data.repository.MovieByGenreRepository
import com.fachmi.privy.mimovie.data.response.ApiResponse
import com.fachmi.privy.mimovie.data.response.MovieByGenreResponse

class MovieByGenreViewModel(private val movieByGenreRepository: MovieByGenreRepository) :
    ViewModel() {

    var genreId: Int? = null

    fun getMovieByGenre(): LiveData<ApiResponse<MovieByGenreResponse>> {
        return movieByGenreRepository.getMovieByGenre(genreId ?: 0)
    }
}