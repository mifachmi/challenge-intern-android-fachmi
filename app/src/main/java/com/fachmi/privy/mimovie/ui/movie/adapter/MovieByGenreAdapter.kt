package com.fachmi.privy.mimovie.ui.movie.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fachmi.privy.mimovie.data.model.GenreModel
import com.fachmi.privy.mimovie.data.model.MovieByGenreModel
import com.fachmi.privy.mimovie.databinding.ItemMovieBinding

class MovieByGenreAdapter : RecyclerView.Adapter<MovieByGenreAdapter.MovieByGenreVH>() {

    private var listener: EventListener? = null
    private val movieList = mutableListOf<MovieByGenreModel>()

    fun submitList(newList: MutableList<MovieByGenreModel>) {
        movieList.apply {
            notifyItemRangeRemoved(0, itemCount)
            clear()
            addAll(newList)
            notifyItemRangeChanged(0, itemCount)
        }
    }

    inner class MovieByGenreVH(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(movie: MovieByGenreModel) {
            with(binding) {
                tvMovieItem.text = movie.movieTitle
                Glide.with(root.context).load("https://image.tmdb.org/t/p/w500${movie.movieImage}")
                    .fitCenter().into(ivMovieItem)
                root.setOnClickListener {
                    listener?.onItemClick(movie)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieByGenreVH {
        val binding = ItemMovieBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return MovieByGenreVH(binding)
    }

    override fun onBindViewHolder(holder: MovieByGenreVH, position: Int) {
        holder.bind(movieList[position])
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    fun setEventListener(listener: EventListener?) {
        this.listener = listener
    }

    interface EventListener {

        fun onItemClick(movie: MovieByGenreModel)
    }
}