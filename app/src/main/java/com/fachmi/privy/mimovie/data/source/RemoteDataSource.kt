package com.fachmi.privy.mimovie.data.source

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fachmi.privy.mimovie.data.network.ApiService
import com.fachmi.privy.mimovie.data.response.ApiResponse
import com.fachmi.privy.mimovie.data.response.GenreResponse
import com.fachmi.privy.mimovie.data.response.MovieByGenreResponse
import com.fachmi.privy.mimovie.data.response.MovieDetailResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class RemoteDataSource private constructor(private val apiService: ApiService) {
    companion object {
        private val TAG = RemoteDataSource::class.java.simpleName.toString()
        private const val ERROR_MESSAGE_IO_EXCEPTION = "Periksa koneksi internet Anda"

        @Volatile
        private var instance: RemoteDataSource? = null

        fun getInstance(apiService: ApiService): RemoteDataSource {
            return instance ?: synchronized(this) {
                instance ?: RemoteDataSource(apiService)
            }
        }
    }

    fun getAllGenre(): LiveData<ApiResponse<GenreResponse>> {
        val result = MutableLiveData<ApiResponse<GenreResponse>>().apply {
            value = ApiResponse.Loading
        }

        apiService.getAllGenre().enqueue(object : Callback<GenreResponse> {
            override fun onResponse(call: Call<GenreResponse>, response: Response<GenreResponse>) {
                val data = response.body()
                println("cek data: $data")
                result.value = if (response.isSuccessful && data != null) {
                    ApiResponse.Success(data)
                } else {
                    ApiResponse.Error(response.message())
                }
            }

            override fun onFailure(call: Call<GenreResponse>, t: Throwable) {
                val errorMessage = if (t is IOException) ERROR_MESSAGE_IO_EXCEPTION
                else t.message.toString()
                result.value = ApiResponse.Error(errorMessage)
                Log.e(TAG, "${call.request().url}\n${t.stackTraceToString()}")
            }

        })

        return result
    }

    fun getMovieByGenre(genreId: Int): LiveData<ApiResponse<MovieByGenreResponse>> {
        val result = MutableLiveData<ApiResponse<MovieByGenreResponse>>().apply {
            value = ApiResponse.Loading
        }

        apiService.getMovieByGenre(genreId.toString())
            .enqueue(object : Callback<MovieByGenreResponse> {
                override fun onResponse(
                    call: Call<MovieByGenreResponse>, response: Response<MovieByGenreResponse>
                ) {
                    val data = response.body()
                    println("cek data: $data")
                    result.value = if (response.isSuccessful && data != null) {
                        ApiResponse.Success(data)
                    } else {
                        ApiResponse.Error(response.message())
                    }
                }

                override fun onFailure(call: Call<MovieByGenreResponse>, t: Throwable) {
                    val errorMessage = if (t is IOException) ERROR_MESSAGE_IO_EXCEPTION
                    else t.message.toString()
                    result.value = ApiResponse.Error(errorMessage)
                    Log.e(TAG, "${call.request().url}\n${t.stackTraceToString()}")
                }

            })

        return result
    }

    fun getMovieDetail(movieId: Int): LiveData<ApiResponse<MovieDetailResponse>> {
        val result = MutableLiveData<ApiResponse<MovieDetailResponse>>().apply {
            value = ApiResponse.Loading
        }

        apiService.getMovieDetail(movieId)
            .enqueue(object : Callback<MovieDetailResponse> {
                override fun onResponse(
                    call: Call<MovieDetailResponse>, response: Response<MovieDetailResponse>
                ) {
                    val data = response.body()
                    println("cek data: $data")
                    result.value = if (response.isSuccessful && data != null) {
                        ApiResponse.Success(data)
                    } else {
                        ApiResponse.Error(response.message())
                    }
                }

                override fun onFailure(call: Call<MovieDetailResponse>, t: Throwable) {
                    val errorMessage = if (t is IOException) ERROR_MESSAGE_IO_EXCEPTION
                    else t.message.toString()
                    result.value = ApiResponse.Error(errorMessage)
                    Log.e(TAG, "${call.request().url}\n${t.stackTraceToString()}")
                }

            })

        return result
    }
}