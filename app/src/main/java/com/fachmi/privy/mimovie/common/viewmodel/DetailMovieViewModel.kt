package com.fachmi.privy.mimovie.common.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.fachmi.privy.mimovie.data.repository.DetailMovieRepository
import com.fachmi.privy.mimovie.data.response.ApiResponse
import com.fachmi.privy.mimovie.data.response.MovieDetailResponse

class DetailMovieViewModel(private val detailMovieRepository: DetailMovieRepository) : ViewModel() {

    var movieId: Int? = null

    fun getDetailMovie(): LiveData<ApiResponse<MovieDetailResponse>> {
        return detailMovieRepository.getDetailMovie(movieId ?: 0)
    }
}