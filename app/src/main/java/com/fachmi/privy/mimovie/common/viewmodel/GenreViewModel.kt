package com.fachmi.privy.mimovie.common.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.fachmi.privy.mimovie.data.repository.GenreRepository
import com.fachmi.privy.mimovie.data.response.ApiResponse
import com.fachmi.privy.mimovie.data.response.GenreResponse

class GenreViewModel(private val genreRepository: GenreRepository) : ViewModel() {

    fun getAllGenre(): LiveData<ApiResponse<GenreResponse>> {
        return genreRepository.getAllGenre()
    }
}