package com.fachmi.privy.mimovie.data.repository

import androidx.lifecycle.LiveData
import com.fachmi.privy.mimovie.data.response.ApiResponse
import com.fachmi.privy.mimovie.data.response.MovieByGenreResponse
import com.fachmi.privy.mimovie.data.source.RemoteDataSource

class MovieByGenreRepository private constructor(private val remoteDataSource: RemoteDataSource) {

    companion object {
        @Volatile
        private var instance: MovieByGenreRepository? = null

        fun getInstance(remoteDataSource: RemoteDataSource): MovieByGenreRepository {
            return instance ?: synchronized(this) {
                instance ?: MovieByGenreRepository(remoteDataSource)
            }
        }
    }

    fun getMovieByGenre(genreId: Int): LiveData<ApiResponse<MovieByGenreResponse>> {
        return remoteDataSource.getMovieByGenre(genreId)
    }
}