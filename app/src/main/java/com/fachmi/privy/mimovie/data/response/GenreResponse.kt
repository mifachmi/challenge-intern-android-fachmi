package com.fachmi.privy.mimovie.data.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GenreResponse(

    @Json(name = "genres")
    val genres: List<GenresItem?>? = null
) {
    @JsonClass(generateAdapter = true)
    data class GenresItem(

        @Json(name = "name")
        val name: String? = null,

        @Json(name = "id")
        val id: Int? = null
    )
}
