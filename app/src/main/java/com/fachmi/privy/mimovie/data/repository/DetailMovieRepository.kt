package com.fachmi.privy.mimovie.data.repository

import androidx.lifecycle.LiveData
import com.fachmi.privy.mimovie.data.response.ApiResponse
import com.fachmi.privy.mimovie.data.response.MovieDetailResponse
import com.fachmi.privy.mimovie.data.source.RemoteDataSource

class DetailMovieRepository private constructor(private val remoteDataSource: RemoteDataSource) {

    companion object {
        @Volatile
        private var instance: DetailMovieRepository? = null

        fun getInstance(remoteDataSource: RemoteDataSource): DetailMovieRepository {
            return instance ?: synchronized(this) {
                instance ?: DetailMovieRepository(remoteDataSource)
            }
        }
    }

    fun getDetailMovie(movieId: Int): LiveData<ApiResponse<MovieDetailResponse>> {
        return remoteDataSource.getMovieDetail(movieId)
    }

}