package com.fachmi.privy.mimovie.ui.movie

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.fachmi.privy.mimovie.common.viewmodel.MovieByGenreViewModel
import com.fachmi.privy.mimovie.common.viewmodel.ViewModelFactory
import com.fachmi.privy.mimovie.data.model.GenreModel
import com.fachmi.privy.mimovie.data.model.MovieByGenreModel
import com.fachmi.privy.mimovie.data.model.toMovieByGenreList
import com.fachmi.privy.mimovie.data.response.ApiResponse
import com.fachmi.privy.mimovie.databinding.ActivityMovieByGenreBinding
import com.fachmi.privy.mimovie.ui.detail.DetailMovieActivity
import com.fachmi.privy.mimovie.ui.movie.adapter.MovieByGenreAdapter

class MovieByGenreActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMovieByGenreBinding
    private lateinit var viewModel: MovieByGenreViewModel
    private var movieByGenreAdapter = MovieByGenreAdapter()

    private lateinit var genreModel: GenreModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieByGenreBinding.inflate(layoutInflater)
        setContentView(binding.root)


        initViewModel()
        getExtras()
        setupAdapter()
        populateData()
    }

    private fun getExtras() {
        intent.apply {
            genreModel = getParcelableExtra("EXTRA_GENRE")!!
            viewModel.genreId = genreModel.genreId
        }
        binding.apply {
            tvMovieByGenre.text = "Movie by ${genreModel.genreName}"
        }
    }

    private fun initViewModel() {
        val factory = ViewModelFactory.getInstance(this)
        viewModel = ViewModelProvider(this, factory)[MovieByGenreViewModel::class.java]
    }

    private fun setupAdapter() {
        binding.rvMovieByGenre.apply {
            layoutManager = GridLayoutManager(this@MovieByGenreActivity, 2)
            adapter = movieByGenreAdapter

            movieByGenreAdapter.setEventListener(object : MovieByGenreAdapter.EventListener {
                override fun onItemClick(movie: MovieByGenreModel) {
                    val intent = Intent(
                        this@MovieByGenreActivity, DetailMovieActivity::class.java
                    ).also { intent ->
                        intent.putExtra("EXTRA_MOVIE", movie)
                    }
                    startActivity(intent)
                }
            })
        }
    }

    private fun populateData() {
        viewModel.getMovieByGenre().observe(this) { result ->
            when (result) {
                is ApiResponse.Loading -> {
                    Toast.makeText(this, "loading...", Toast.LENGTH_SHORT).show()
                }
                is ApiResponse.Success -> {
                    val movieList = result.data.toMovieByGenreList()
                    movieByGenreAdapter.submitList(movieList)
                }
                is ApiResponse.Error -> {
                    Toast.makeText(this, result.errorMessage, Toast.LENGTH_SHORT)
                        .show()
                }
                else -> {}
            }
        }
    }
}