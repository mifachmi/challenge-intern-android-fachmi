package com.fachmi.privy.mimovie.common.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fachmi.privy.mimovie.data.repository.DetailMovieRepository
import com.fachmi.privy.mimovie.data.repository.GenreRepository
import com.fachmi.privy.mimovie.data.repository.MovieByGenreRepository
import com.fachmi.privy.mimovie.di.Injection

class ViewModelFactory private constructor(
    private val genreRepository: GenreRepository,
    private val movieByGenreRepository: MovieByGenreRepository,
    private val detailMovieRepository: DetailMovieRepository
) : ViewModelProvider.NewInstanceFactory() {

    companion object {
        @Volatile
        private var instance: ViewModelFactory? = null

        fun getInstance(context: Context): ViewModelFactory {
            return instance ?: synchronized(this) {
                instance ?: ViewModelFactory(
                    Injection.provideGenreRepository(context),
                    Injection.provideMovieByGenreRepository(context),
                    Injection.provideDetailMovieRepository(context)
                )
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        when {
            modelClass.isAssignableFrom(GenreViewModel::class.java) -> {
                GenreViewModel(genreRepository) as T
            }
            modelClass.isAssignableFrom(MovieByGenreViewModel::class.java) -> {
                MovieByGenreViewModel(movieByGenreRepository) as T
            }
            modelClass.isAssignableFrom(DetailMovieViewModel::class.java) -> {
                DetailMovieViewModel(detailMovieRepository) as T
            }
            else -> throw Throwable("Unknown ViewModel class: ${modelClass.name}")
        }
}