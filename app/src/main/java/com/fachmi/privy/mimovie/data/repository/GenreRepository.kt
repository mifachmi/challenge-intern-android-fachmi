package com.fachmi.privy.mimovie.data.repository

import androidx.lifecycle.LiveData
import com.fachmi.privy.mimovie.data.response.ApiResponse
import com.fachmi.privy.mimovie.data.response.GenreResponse
import com.fachmi.privy.mimovie.data.source.RemoteDataSource

class GenreRepository private constructor(private val remoteDataSource: RemoteDataSource) {

    companion object {
        @Volatile
        private var instance: GenreRepository? = null

        fun getInstance(remoteDataSource: RemoteDataSource): GenreRepository {
            return instance ?: synchronized(this) {
                instance ?: GenreRepository(remoteDataSource)
            }
        }
    }

    fun getAllGenre(): LiveData<ApiResponse<GenreResponse>> {
        return remoteDataSource.getAllGenre()
    }

}