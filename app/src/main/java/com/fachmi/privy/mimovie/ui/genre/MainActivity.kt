package com.fachmi.privy.mimovie.ui.genre

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fachmi.privy.mimovie.common.viewmodel.GenreViewModel
import com.fachmi.privy.mimovie.common.viewmodel.ViewModelFactory
import com.fachmi.privy.mimovie.data.model.GenreModel
import com.fachmi.privy.mimovie.data.model.toGenreList
import com.fachmi.privy.mimovie.data.response.ApiResponse
import com.fachmi.privy.mimovie.databinding.ActivityMainBinding
import com.fachmi.privy.mimovie.ui.genre.adapter.GenreAdapter
import com.fachmi.privy.mimovie.ui.movie.MovieByGenreActivity

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private var genreAdapter = GenreAdapter()
    private lateinit var viewModel: GenreViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViewModel()
        setupAdapter()
        populateDataGenre()
    }

    private fun initViewModel() {
        val factory = ViewModelFactory.getInstance(this)
        viewModel = ViewModelProvider(this, factory)[GenreViewModel::class.java]
    }

    private fun setupAdapter() {
        binding.rvGenre.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = genreAdapter

            genreAdapter.setEventListener(object : GenreAdapter.EventListener {
                override fun onItemClick(genre: GenreModel) {
                    val intent =
                        Intent(this@MainActivity, MovieByGenreActivity::class.java).also { intent ->
                            intent.putExtra("EXTRA_GENRE", genre)
                        }
                    startActivity(intent)
                }
            })
        }
    }

    private fun populateDataGenre() {
        viewModel.getAllGenre().observe(this) { result ->
            when (result) {
                is ApiResponse.Loading -> {
                    Toast.makeText(this@MainActivity, "loading...", Toast.LENGTH_SHORT).show()
                }
                is ApiResponse.Success -> {
                    val genreList = result.data.toGenreList()
                    genreAdapter.submitList(genreList)
                }
                is ApiResponse.Error -> {
                    Toast.makeText(this@MainActivity, result.errorMessage, Toast.LENGTH_SHORT)
                        .show()
                }
                else -> {}
            }
        }
    }
}